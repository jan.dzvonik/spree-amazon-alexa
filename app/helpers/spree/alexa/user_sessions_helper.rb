module Spree
  module Alexa
    module UserSessionsHelper
      def flash_feedback(flash)
        if flash.present?
          close_button = button_tag(class: 'close', 'data-dismiss' => 'alert', 'aria-label' => Spree.t(:close)) do
            content_tag('span', '&times;'.html_safe, 'aria-hidden' => true)
          end

          message = flash[:error] || flash[:notice] || flash[:success]

          flash_class = 'danger' if flash[:error]
          flash_class = 'info' if flash[:notice]
          flash_class = 'success' if flash[:success]

          if message.is_a?(Array)
            content_tag :div, class: 'col-md-6 col-md-offset-3' do
              message.each do |m|
                concat content_tag(:div, (close_button + m), class: "alert alert-#{flash_class} alert-auto-disappear")
              end
            end
          else
            flash_div = content_tag(:div, (close_button + message), class: "alert alert-#{flash_class} alert-auto-disappear")
            content_tag(:div, flash_div, class: 'col-md-6 col-md-offset-3')
          end
        end
      end
    end
  end
end
