class Spree::AlexaAccountLinkingSession < Spree::Base
  include ActiveModel::Validations

  before_validation(on: :create) do
    self.session_id = SecureRandom.hex(24)
  end

  validate :check_redirect_uri
  validate :check_response_type
  validate :check_client_id
  validates_presence_of :state, :message => "Missing Alexa state parameter"

  private

  def check_redirect_uri
    errors.add(:redirect_uri, "Invalid Alexa redirect URI") unless [
      Rails.configuration.alexa_redirect_uri_1,
      Rails.configuration.alexa_redirect_uri_2,
      Rails.configuration.alexa_redirect_uri_3
    ].include? redirect_uri
  end

  def check_response_type
    if response_type != "token"
      errors.add(:response_type, "Unsupported Alexa account linking method")
    end
  end

  def check_client_id
    if client_id != Rails.configuration.alexa_client_id
      errors.add(:client_id, "Invalid Alexa client id")
    end
  end
end
