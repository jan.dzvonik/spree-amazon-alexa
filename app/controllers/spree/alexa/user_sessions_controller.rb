class Spree::Alexa::UserSessionsController < Devise::SessionsController
  helper 'spree/base'

  include Spree::Core::ControllerHelpers::Auth
  include Spree::Core::ControllerHelpers::Common
  include Spree::Core::ControllerHelpers::Store

  layout "spree/layouts/alexa_account_linking"

  def new
    @alexa_params_valid = true

    begin
      params.require([
        :state,
        :client_id,
        :response_type,
        :redirect_uri
      ])

      alexa_session = Spree::AlexaAccountLinkingSession.new(
        client_id: params[:client_id],
        state: params[:state],
        redirect_uri: params[:redirect_uri],
        response_type: params[:response_type]
      )

      alexa_session.save!
      @alexa_session_id = alexa_session[:session_id]

    rescue ActionController::ParameterMissing
      @alexa_params_valid = false
      flash.now[:error] = "Required parameter for Alexa account linking is missing"
      return

    rescue ActiveRecord::RecordInvalid
      @alexa_params_valid = false
      errors = []
      alexa_session.errors.each do |attribute, message|
        errors << message
      end
      flash.now[:error] = errors
      return
    end
  end

  # For implicit grant, include the
  #   state
  #   access_token
  #   token_type
  # in the URL fragment. The token_type should be Bearer.
  def create
    @alexa_params_valid = true

    begin
      params.require(:session_id)
    rescue ActionController::ParameterMissing
      @alexa_params_valid = false
      flash.now[:error] = "Incorrect use of application"
      redirect_to :new and return
    end

    alexa_params = Spree::AlexaAccountLinkingSession.where(session_id: params[:session_id]).take

    authenticate_spree_user!

    if spree_user_signed_in?
      if try_spree_current_user.spree_api_key.blank?
        try_spree_current_user.generate_spree_api_key!
      end

      uri = alexa_params[:redirect_uri]
      state = alexa_params[:state]
      token_type = "Bearer"
      api_key = try_spree_current_user[:spree_api_key]

      # reditect to this URI
      @account_linking_uri = "#{uri}#state=#{state}&access_token=#{api_key}&token_type=#{token_type}"
      sign_out_and_redirect(try_spree_current_user)
    else
      @alexa_session_id = params[:session_id]
      respond_to do |format|
        format.html {
          flash.now[:error] = t('devise.failure.invalid')
          render :new
        }
        format.js {
          render json: { error: t('devise.failure.invalid') }, status: :unprocessable_entity
        }
      end
    end
  end

  protected
    def after_sign_out_path_for(scope)
      logger.info "  alexia account linking scope: #{scope}"

      if scope == :spree_user
        # URI defined in create method
        @account_linking_uri
      else
        root_path
      end
    end

    def translation_scope
      'devise.user_sessions'
    end

    private

    def accurate_title
      Spree.t(:login)
    end

end
