Spree::Core::Engine.add_routes do
  namespace :alexa, path: 'alexa' do
    devise_scope :spree_user do
      get '/login' => 'user_sessions#new', :as => :login
      post '/login' => 'user_sessions#create', :as => :account_linking
    end
  end
end
